
package prak3;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class no3 extends JFrame  {
    public no3 () {
       this.setSize (300,500);
       this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       this.setTitle("ini Class turunan dari class JFrame");
       this.setVisible(true);
       this.setTitle("FIND");
    
       
        JPanel panel = new JPanel();
        JButton tombol = new JButton();
        tombol.setText("ini tombol");
        panel.add(tombol);
        this.add(panel);
        
        JLabel label = new JLabel();
        JButton masuk = new JButton();
        label.setText("masuk");
        panel.add(masuk);
        this.add(panel);
        
        JCheckBox c = new JCheckBox();
        c.setText("Squad");
        panel.add(c);
        this.add(panel);
        
        JRadioButton r = new JRadioButton();
        r.setText("Berita Kehilangan");
        panel.add(r);
        this.add(panel);
        
    }
    public static void main(String[] args) {
        new no3();
    }
}
