package prak3c;
import java.awt.Container;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
public class no3 extends JDialog {
    private final JLabel image;
    private final JLabel text;
    private static final int FRAME_WIDTH = 100;
    private static final int FRAME_HEIGHT = 300;
    public no3() {
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Text and Icon Label");
        contentPane.setLayout(null);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        text = new JLabel("GRAPE");
        text.setBounds(38, 100, 99, 199);
        contentPane.add(text);
        ImageIcon img = new ImageIcon("grape.png");
        image = new JLabel();
        image.setIcon(img);
        image.setBounds(5,10,200,200);
        contentPane.add(image);
    }
    public static void main(String[] args) {
        no3 dialog = new no3();
        dialog.setVisible(true);
    }
}
