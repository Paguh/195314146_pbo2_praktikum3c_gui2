package prak3c;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
public class no6 extends JDialog {
private final JComboBox box;
private final JLabel image,text;
private final JList list;
private final JPanel listPanel;
private static final int FRAME_WIDTH = 500;
private static final int FRAME_HEIGHT = 250;
private static final int BUTTON_WIDTH = 484;
private static final int BUTTON_HEIGHT = 30;
public no6 () {
String[] names = {"Kanada",
"dengan nama asli Dominion of Canada "
, " ",
"merupakan negara paling utara di kawasan Amerika Utara. Kerajaan (monarki konstitusional) ini terbagi dalam 10 provinsi dan 3 teritori",
"dengan menganut sistem desentralisasi. Kanada berdiri pada tahun 1867, setelah disahkannya undang-undang Konfederasi. ",
"Ottawa menjadi ibukota negara ini. Kantor Gubernur Jenderal, Perdana Menteri, serta parlemen nasional terletak di kota tersebut. ",
"Kanada pernah menjadi jajahan Prancis; yang kemudian direbut oleh Inggris, hingga hari kemerdekaannya.",
"Kini, negara ini menjadi anggota La Francophonie, sekaligus negara Persemakmuran Inggris. Luas Kanada mencapai 9.970.610 km². ",
"Negara ini tergolong negara maju, yang perekonomiannya bergantung pada hasil alam.Kanada merupakaan monarki konstitusional, ",
"dengan pemerintahan yang berbentuk demokrasi parlementer federal. Ratu Elizabeth II menjadi kepala negara. "};
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("ComboBoxDemo");
        contentPane.setLayout(null);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        ImageIcon img = new ImageIcon("canada.png");
        image = new JLabel();
        image.setIcon(img);
        image.setBounds(10,50,200,100);
        contentPane.add(image);
        box = new JComboBox();
        box.addItem("Canada");
        box.setBounds(1,1,BUTTON_WIDTH,BUTTON_HEIGHT);
        contentPane.add(box);
        text = new JLabel("Canada");
        text.setBounds(85, 100, 100, 150);
        contentPane.add(text);
        listPanel = new JPanel(new GridLayout(0, 1));
        list = new JList(names);
        listPanel.add(new JScrollPane(list));
        listPanel.setBounds(250,50,230,150);
        contentPane.add(listPanel);
       } 
        public static void main(String[] args) {
        no6 dialog = new no6 ();
        dialog.setVisible(true);
    }
}

