package prak3c;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
public class no7 extends JDialog {
private final JList list;
private final JPanel listPanel; 
private JLabel image;
private static final int FRAME_WIDTH = 500;
private static final int FRAME_HEIGHT = 200;
public no7 () {
 String[] names = {"Kanada","Jerman","Indonesia","Malaysia","Jepang","Spanyol","Vietnam","Italy"
                   ,"Portugal","Kameron","Inggris"};
  Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("ListDemo");
        contentPane.setLayout(null);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);  
        ImageIcon img = new ImageIcon("spanyol.png");
        image = new JLabel();
        image.setIcon(img);
        image.setBounds(110,50,200,100);
        contentPane.add(image);
        ImageIcon i = new ImageIcon("japan.png");
        image = new JLabel();
        image.setIcon(i);
        image.setBounds(110,10,200,50);
        contentPane.add(image);
        ImageIcon m = new ImageIcon("malay.png");
        image = new JLabel();
        image.setIcon(m);
        image.setBounds(230,10,200,50);
        contentPane.add(image);
        ImageIcon a = new ImageIcon("indo.png");
        image = new JLabel();
        image.setIcon(a);
        image.setBounds(230,50,200,100);
        contentPane.add(image);
        ImageIcon g = new ImageIcon("jerman.png");
        image = new JLabel();
        image.setIcon(g);
        image.setBounds(350,50,200,100);
        contentPane.add(image);
        ImageIcon e = new ImageIcon("canada2.png");
        image = new JLabel();
        image.setIcon(e);
        image.setBounds(350,10,200,50);
        contentPane.add(image);
        listPanel = new JPanel(new GridLayout(0, 1));
        list = new JList(names);
        listPanel.add(new JScrollPane(list));
        listPanel.setBounds(1,1,100,160);
        contentPane.add(listPanel);
}
  public static void main(String[] args) {
        no7 dialog = new no7 ();
        dialog.setVisible(true);
}
}