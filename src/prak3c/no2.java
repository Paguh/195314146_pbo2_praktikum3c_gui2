package prak3c;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JDialog;
public class no2 extends JDialog {
    private final JButton b1;
    private final JButton b2;
    private final JButton b3;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 30;
    public no2() {
        Container contentPane = getContentPane();
        setSize(350,250);
        setResizable(true);
        setTitle("Button Test");
        contentPane.setLayout(null);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        b1 = new JButton("Yellow");
        b1.setBounds(30, 10, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(b1);
        b2 = new JButton("Blue");
        b2.setBounds(130,10, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(b2);
        b3 = new JButton("Red");
        b3.setBounds(230,10, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(b3);  
    }
    public static void main(String[] args){
        no2 dialog = new no2();
        dialog.setVisible(true);
    }   
}
