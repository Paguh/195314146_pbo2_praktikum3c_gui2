package prak3c;
import java.awt.Color;
import java.awt.Container;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
public class no5 extends JDialog {
    private final JButton left;
    private final JButton right;
    private final JCheckBox c,b,i;
    private final JRadioButton radio,radio2,radio3;
    private final JTextArea text;
    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 300;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 30;
    public no5() {
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("RadioButtonDemo");
        contentPane.setLayout(null);
        contentPane.setBackground(Color.WHITE);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        text = new JTextArea("Welcome to java");
        text.setBounds(190, 100, 150, 50);
        contentPane.add(text);
        left = new JButton("Left");
        left.setBounds(150, 200, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(left);
        right = new JButton("Right");
        right.setBounds(250, 200, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(right);
        c = new JCheckBox("Centreed");
        c.setBounds(400, 60, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(c);
        b = new JCheckBox("Bold");
        b.setBounds(400, 85, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(b);
        i = new JCheckBox("Italic");
        i.setBounds(400, 110, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(i);
        radio = new JRadioButton("Red");
        radio.setBounds(2, 60,BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(radio);
        radio2 = new JRadioButton("Green");
        radio2.setBounds(2, 85, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(radio2);
        radio3 = new JRadioButton("Blue");
        radio3.setBounds(2, 110, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(radio3);
        ButtonGroup g = new ButtonGroup();
        g.add(radio);
        g.add(radio2);
        g.add(radio3);
    }
    public static void main(String[] args) {
        no5 dialog = new no5();
        dialog.setVisible(true);
    }
}
