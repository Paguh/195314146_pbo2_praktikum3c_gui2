package prak3b;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class ch14AbsolutePositioning extends JFrame {
private static final int FRAME_WIDTH = 300;
private static final int FRAME_HEIGHT = 200;
private static final int FRAME_X_ORIGIN = 300;
private static final int FRAME_Y_ORIGIN = 100;
private static final int BUTTON_WIDTH = 80;
private static final int BUTTON_HEIGHT = 40;
private JButton cancelButton;
private JButton okButton;
private JTextField txtField;

public static void main(String[] args) {
       ch14AbsolutePositioning frame = new ch14AbsolutePositioning ();
       frame.setVisible(true);
    }

public ch14AbsolutePositioning(){
    Container contentPane = getContentPane ();
    setSize(FRAME_WIDTH,FRAME_HEIGHT);
    setResizable(true);
    setTitle("Program ch14AbsolutePositioning");
    setLocation(FRAME_X_ORIGIN,FRAME_Y_ORIGIN);
    contentPane.setBackground(Color.WHITE);
    
    okButton = new JButton ("OK");
    okButton.setBounds(110,125,BUTTON_WIDTH,BUTTON_HEIGHT);
    contentPane.add(okButton);
    
    cancelButton = new JButton ("Cancel");
    cancelButton.setBounds(160,125,BUTTON_WIDTH,BUTTON_HEIGHT);
    contentPane.add(cancelButton);
    
    setDefaultCloseOperation(EXIT_ON_CLOSE);
}
}
